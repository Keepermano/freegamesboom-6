# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **options):

        call_command('makemessages', **{'locale': [x[0] if x[0] != 'zh-hans' else 'zh_Hans' for x in settings.LANGUAGES]})
