# -*- coding: utf-8 -*-
from tqdm import tqdm
from django.conf import settings
import requests
from django.core.management.base import BaseCommand
from catalog.models import Game, GameVideo, GamesTranslation


class Command(BaseCommand):

    def handle(self, *args, **options):
        finish = False
        # Game.objects.all().update(video='', video_en='', video_ru='', video_ar='', video_youtube_processed=False)
        games = Game.objects.filter(video_youtube_processed=False).order_by('-play_counter')
        game_videos_en = {obj.id: obj for obj in GamesTranslation.objects.filter(language_code='en', text_block_code='video')}
        game_videos_ru = {obj.id: obj for obj in GamesTranslation.objects.filter(language_code='ru', text_block_code='video')}

        update_objects = []
        create_objects = []
        counter = 0
        for game in tqdm(games, disable=False):

            queries = (
                'game %s walkthrough video' % game.name_en,
                'онлайн игра %s прохождение' % game.name_ru,
                # '%s  اجتاز اللعبة اون لاين ' % o.name_ar
            )
            for idx, q in enumerate(queries):
                url = 'https://www.googleapis.com/youtube/v3/search' \
                      '?part=snippet&maxResults=10&order=relevance&' \
                      'safeSearch=moderate&q=%s&key=%s' % (q, settings.YOUTUBE_KEY)
                r = requests.get(url)
                if r.status_code == 200:
                    results = []
                    data = r.json()['items']

                    video_en_obj = game_videos_en.get(game.id)
                    video_ru_obj = game_videos_ru.get(game.id)
                    value_en = video_en_obj.text if video_en_obj else ''
                    value_ru = video_ru_obj.text if video_ru_obj else ''

                    for item in data:
                        try:
                            results.append(item['id']['videoId'])
                        except KeyError:
                            continue
                    for c in results[:3]:
                        if idx == 0:
                            value_en += "%s\n" % c
                            # o.video_en += "%s\n" % c
                        elif idx == 1:
                            value_ru += "%s\n" % c
                    if value_en:
                        if video_en_obj and value_en != video_en_obj.text:
                            video_en_obj.text = value_en
                            update_objects.append(video_en_obj)
                        elif not video_en_obj:
                            create_objects.append(GamesTranslation(
                                text=value_en,
                                language_code='en',
                                text_block_code='video',
                                game=game
                            ))
                    if value_ru:
                        if video_ru_obj and value_ru != video_ru_obj.text:
                            video_ru_obj.text = value_ru
                            update_objects.append(video_ru_obj)
                        elif not video_ru_obj:
                            create_objects.append(GamesTranslation(
                                text=value_ru,
                                language_code='ru',
                                text_block_code='video',
                                game=game
                            ))
                    game.video_youtube_processed = True
                    game.save()
                    counter += 1
                else:
                    finish = True
                    break
            if finish:
                break
        GamesTranslation.objects.bulk_update(update_objects, fields=['text'], batch_size=5000)
        GamesTranslation.objects.bulk_create(create_objects, ignore_conflicts=True, batch_size=5000)
        print(counter)
