# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from tqdm import tqdm

from catalog.models import Category, Tag, Game


class Command(BaseCommand):

    def handle(self, *args, **options):

        # for o in tqdm(Category.objects.all(), disable=False):
        #     if o.description_en:
        #         try:
        #             o.seo_top = o.description.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #             o.seo_top_en = o.description_en.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #         except IndexError:
        #             pass
        #     if o.description_ru:
        #         try:
        #             o.seo_top_ru = o.description_ru.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #         except IndexError:
        #             pass
        #     if o.description_ar:
        #         try:
        #             o.seo_top_ar = o.description_ar.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #         except IndexError:
        #             pass
        #     o.save()
        #
        # for o in tqdm(Tag.objects.all(), disable=False):
        #     if o.description_en:
        #         try:
        #             o.seo_top = o.description.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #             o.seo_top_en = o.description_en.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #         except IndexError:
        #             pass
        #     if o.description_ru:
        #         try:
        #             o.seo_top_ru = o.description_ru.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #         except IndexError:
        #             pass
        #     if o.description_ar:
        #         try:
        #             o.seo_top_ar = o.description_ar.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #         except IndexError:
        #             pass
        #     o.save()
        #
        # for o in tqdm(Game.objects.all(), disable=False):
        #     if o.description_en:
        #         try:
        #             o.seo_top = o.description.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #             o.seo_top_en = o.description_en.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #         except IndexError:
        #             pass
        #     if o.description_ru:
        #         try:
        #             o.seo_top_ru = o.description_ru.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #         except IndexError:
        #             pass
        #     if o.description_ar:
        #         try:
        #             o.seo_top_ar = o.description_ar.split('<p>')[1].replace('</p>', '').replace('\r\n\r\n', '')
        #         except IndexError:
        #             pass
        #     o.save()

        for o in tqdm(Category.objects.all(), disable=False):
            most_popular_game = o.games_published.all().exclude(image='').order_by('-play_counter').first()
            if most_popular_game:
                o.image = most_popular_game.image
                o.save()

        for o in tqdm(Tag.objects.all(), disable=False):
            most_popular_game = o.games_published.all().exclude(image='').order_by('-play_counter').first()
            if most_popular_game:
                o.image = most_popular_game.image
                o.save()
