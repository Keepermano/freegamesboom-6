from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from .models import Banner, Counter, MenuButton, MenuFooter, MenuHead, Settings, SiteFile, SettingsTranslation


class TranslationTabs(TranslationAdmin):
    class Media:
        js = (
            'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }


class SiteFileInline(admin.TabularInline):
    model = SiteFile
    extra = 0


class CounterInline(admin.TabularInline):
    model = Counter
    extra = 0


@admin.register(SettingsTranslation)
class TranslationsAdmin(admin.ModelAdmin):
    list_filter = ['language_code']


@admin.register(Settings)
class SettingsAdmin(TranslationTabs):
    inlines = (SiteFileInline, CounterInline)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(MenuHead, MenuFooter, MenuButton)
class MenuAdmin(TranslationTabs):
    list_display = ('title', 'url', 'position')
    list_editable = ('url', 'position')


@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    pass
